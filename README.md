# geeking-common-utils

#### 介绍

Java 常用工具类集合，有部分代码或者类摘录自优秀开源项目，方便日常开发资料查找

#### 摘录自：(包含但不仅限于，无名次先后)
1. [google.guava](https://github.com/google/guava) 
2. [aws-java-sdk](https://github.com/aws/aws-sdk-java) 
3. [apache-commons-lang](https://github.com/apache/commons-lang)


#### 安装教程
```shell script
$ git clone git@gitee.com:devilscode/geeking-common-utils.git
$ cd geeking-common-utils & mvn clean install
```


#### 使用说明
```xml
<dependency>
    <groupId>com.mohist.simplify</groupId>
    <artifactId>common-lang-utils</artifactId>
    <version>${mohist.utils.version}</version>
</dependency>
```

#### 参与贡献
暂时无法参与，后期有此计划，接受投稿和issue