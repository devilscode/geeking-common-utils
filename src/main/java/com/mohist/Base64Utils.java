package com.mohist;

import java.nio.ByteBuffer;
import java.util.Base64;

/**
 * <p>
 *
 * </p>
 *
 * @author Wilton Jia
 * @date 2020-09-30
 * @since
 */
public class Base64Utils {

    /**
     * Construct Base64.Encoder
     */
    private static Base64.Encoder encoder = Base64.getEncoder();
    /**
     * Construct Base64.Decoder
     */
    private static Base64.Decoder decoder = Base64.getDecoder();

    /**
     * Return a String in base64 when Given a byte array
     */
    public static String encodeToString(byte [] data){
        return encoder.encodeToString(data);
    }

    /**
     * Return byte array in base64 when Given a byte array
     */
    public static byte [] encode(byte [] data){
        return encoder.encode(data);
    }

    /**
     * Return int when Given 2 byte arrays
     */
    public static int encode(byte [] src, byte [] dest){
        return encoder.encode(src, dest);
    }

    /**
     * Return a ByteBuffer in base64 when Given a ByteBuffer
     */
    public static ByteBuffer encode(ByteBuffer buffer){
        return encoder.encode(buffer);
    }

    /**
     * Return a byte array in base64 when Given a String
     */
    public static byte [] decode(String data){
        return decoder.decode(data);
    }

    /**
     * Return byte array when Given a byte array in base64
     */
    public static byte [] decode(byte [] data){
        return decoder.decode(data);
    }

    /**
     * Return a ByteBuffer when Given a ByteBuffer in base64
     */
    public static ByteBuffer decode(ByteBuffer buffer){
        return decoder.decode(buffer);
    }

    /**
     * Return int when Given 2 byte arrays
     */
    public static int decode(byte [] src, byte [] dest){
        return decoder.decode(src, dest);
    }
}
