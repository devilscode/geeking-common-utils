package com.mohist;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author Wilton Jia
 * @date 2020-09-29
 * @since
 */
public class DateUtils {

    /**
     * yyyy/MM/dd
     */
    private final static SimpleDateFormat exportedSimpleDateFormat= new SimpleDateFormat("yyyy/MM/dd");

    /**
     * yyyy-MM-dd
     */
    private final static SimpleDateFormat normalSimpleDateFormat= new SimpleDateFormat("yyyy-MM-dd");

    /**
     * yyyy-MM-dd HH:mm:ss
     */
    private final static SimpleDateFormat fullTimeSimpleDateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");



    public static String format(Date date, String format){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }


    public static Date parse(String date, String format) throws ParseException {
        if(StringUtils.isBlank(date)) {
            return new Date();
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.parse(date);
    }


    public static String formatExportedDate(Date date) {
        return exportedSimpleDateFormat.format(date);
    }

    public static String formatNormalDate(Date date) {
        return normalSimpleDateFormat.format(date);
    }

    public static String formatFullDate(Date date) {
        return fullTimeSimpleDateFormat.format(date);
    }

    public static SimpleDateFormat getExportedSimpleDataFormat() {
        return exportedSimpleDateFormat;
    }

    public static Date parseFullSimpleDataFormat(String date) {
        if(StringUtils.isBlank(date)) {
            return new Date();
        }
        try {
            return fullTimeSimpleDateFormat.parse(date);
        } catch (ParseException e) {
            throw new RuntimeException(String.format(INVALID_DATE_MESSAGE, date));
        }
    }

    public static Date parseExportedSimpleDataFormat(String date) {
        try {
            return exportedSimpleDateFormat.parse(date);
        } catch (ParseException e) {
            throw new RuntimeException(String.format(INVALID_DATE_MESSAGE, date));
        }
        catch(Exception e) {
            throw new RuntimeException(String.format(INVALID_DATE_MESSAGE, date),e);
        }
    }

    public static SimpleDateFormat getNormalSimpleDataFormat() {
        return normalSimpleDateFormat;
    }

    public static Date parseNormalSimpleDataFormat(String date) {
        try {
            return normalSimpleDateFormat.parse(date);
        } catch (ParseException e) {
            throw new RuntimeException(String.format(INVALID_DATE_MESSAGE, date));
        }
        catch(Exception e) {
            throw new RuntimeException(String.format(INVALID_DATE_MESSAGE, date),e);
        }
    }

    private static final String INVALID_DATE_MESSAGE="Invalid date\t%s";



}
