package com.mohist;

import java.io.*;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * <p>
 *
 * </p>
 *
 * @author Wilton Jia
 * @date 2020-09-29
 * @since
 */
public class MD5Utils {

    private static final String[] HEX_DIGITS = { "0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };

    private static final int SIXTEEN_K = 1 << 14;
    /**
     * Computes the MD5 hash of the data in the given input stream and returns
     * it as an array of bytes.
     * Note this method closes the given input stream upon completion.
     */
    public static byte[] computeMD5Hash(InputStream is) throws IOException {
        BufferedInputStream bis = new BufferedInputStream(is);
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            byte[] buffer = new byte[SIXTEEN_K];
            int bytesRead;
            while ( (bytesRead = bis.read(buffer, 0, buffer.length)) != -1 ) {
                messageDigest.update(buffer, 0, bytesRead);
            }
            return messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(e);
        } finally {
            try {
                bis.close();
            } catch (Exception e) {
                System.out.println("Unable to close input stream of hash candidate: " + e);
            }
        }
    }

    /**
     * Returns the MD5 in base64 for the data from the given input stream.
     * Note this method closes the given input stream upon completion.
     */
    public static String md5AsHexString(InputStream is) throws IOException {
        return byteArrayToHexString(computeMD5Hash(is));
    }

    /**
     * Computes the MD5 hash of the given data and returns it as an array of
     * bytes.
     */
    public static byte[] computeMD5Hash(byte[] input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            return md.digest(input);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Returns the MD5 in hex for the given byte array.
     */
    public static String md5AsHexString(byte[] input) {
        return byteArrayToHexString((computeMD5Hash(input)));
    }

    /**
     * Computes the MD5 of the given file.
     */
    public static byte[] computeMD5Hash(File file) throws FileNotFoundException, IOException {
        return computeMD5Hash(new FileInputStream(file));
    }

    /**
     * Returns the MD5 in hex for the given file.
     */
    public static String md5AsHexString(File file) throws FileNotFoundException, IOException {
        return byteArrayToHexString(computeMD5Hash(file));
    }

    /**
     * Return a normal MD5 String of given String, without base64 encode
     */
    public static String md5AsHexString(String original){
        if(original != null){
            byte [] data = original.getBytes(Charset.defaultCharset());
            return byteArrayToHexString(computeMD5Hash(data));
        }
        return null;
    }

    /**
     * Computes a byte array to hex and Returns a String
     */
    private static String byteArrayToHexString(byte [] data) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            int n = data[i];
            if (n < 0) {
                n += 256;
            }
            int d1 = n / 16;
            int d2 = n % 16;
            stringBuffer.append(HEX_DIGITS[d1] + HEX_DIGITS[d2]);
        }
        return stringBuffer.toString();
    }



}
