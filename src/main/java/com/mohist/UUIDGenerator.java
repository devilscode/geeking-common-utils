package com.mohist;

import java.util.UUID;

/**
 * <p>
 *      UUID creator
 * </p>
 *
 * @author Wilton Jia
 * @date 2020-09-29
 * @version 1.0
 */
public class UUIDGenerator {


    /**
     * Return an UUID String
     */
    public static String generate(){
        return UUID.randomUUID().toString();
    }


    /**
     * Return an UUID String without '-'
     * @return
     */
    public static String generateWithout_(){
        return generate().replace("-", "");
    }


}
