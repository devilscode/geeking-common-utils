package com.mohist;

import java.io.*;
import java.nio.channels.FileChannel;

/**
 * <p>
 *
 * </p>
 *
 * @author jiawentao
 * @date 2020-10-09
 * @since
 */
public class FileUtils {


    /**
     * Read a file to byte array, Provide a path ,returns a byte array
     * @param path abs file path
     */
    public static byte [] read(String path){
        byte[] fileBytes = null;
        File file = new File(path);
        try(FileInputStream fileInputStream = new FileInputStream(file);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()){
            int len;
            byte[] buffer = new byte[1024];
            //read file to byte array
            while ((len = fileInputStream.read(buffer)) != -1) {
                // start with off, write len bytes to array
                byteArrayOutputStream.write(buffer, 0, len);
            }
            // copy to the result
            fileBytes = byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileBytes;
    }


    /**
     * Write data to a file
     */
    public static void write(byte [] data, String fileName){
        try(ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(data);
            FileOutputStream out = new FileOutputStream(fileName)){
            byte[] buff = new byte[1024];
            int len = 0;
            while((len=byteArrayInputStream.read(buff))!=-1){
                out.write(buff, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Copy file to another path
     */
    public static void copy(String source, String target){

        try(FileInputStream fileInputStream = new FileInputStream(source)) {
            copy(fileInputStream, target);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * Write file to another path
     */
    public static void copy(FileInputStream inputStream, String target){
        try(FileChannel inputChannel = inputStream.getChannel();
            FileChannel outputChannel = new FileOutputStream(target).getChannel()){
            outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
