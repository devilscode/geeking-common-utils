package com.nohist.test;

import com.mohist.MD5Utils;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * <p>
 *
 * </p>
 *
 * @author jiawentao
 * @date 2020-09-30
 * @since
 */
public class MD5UtilsTest {


    @Test
    public void testMD5AsBase64() throws IOException {
        String s = MD5Utils.md5AsHexString(new File("/data/1.png"));
        System.out.println(s);

        String s1 = MD5Utils.md5AsHexString(new FileInputStream("/data/1.png"));
        System.out.println(s1);

        String s2 = MD5Utils.md5AsHexString("123456");
        System.out.println(s2);
    }






}
