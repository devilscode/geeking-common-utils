package com.nohist.test;

import com.mohist.FileUtils;
import org.junit.Test;

/**
 * <p>
 *
 * </p>
 *
 * @author jiawentao
 * @date 2020-10-09
 * @since
 */
public class FileUtilsTest {


    private static final String path = "/data/";


    @Test
    public void test1(){
        String data = "this is data";
        FileUtils.write(data.getBytes(), path+"111.txt");
    }

    @Test
    public void test2(){
        byte[] read = FileUtils.read(path + "111.txt");
        System.out.println(new String(read));
    }

    @Test
    public void test3(){
        FileUtils.copy(path+"111.txt", path+"222.txt");
    }


}
