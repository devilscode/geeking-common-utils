package com.nohist.test;

import com.mohist.Base64Utils;
import com.mohist.RSAEncrypt;
import org.junit.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Map;

/**
 * <p>
 *
 * </p>
 *
 * @author jiawentao
 * @date 2020-10-09
 * @since
 */
public class RSAEncryptTest {


    private static String path = "/data/";


    @Test
    public void test1() throws Exception {
        Map<String, String> keyPair = RSAEncrypt.genKeyPair(path);
        for (String s : keyPair.keySet()) {
            System.out.printf("%s: %s", s, keyPair.get(s));
            //4bfda50f21e540f19ab394218e62ccb7_privateKey.pem
            //4bfda50f21e540f19ab394218e62ccb7_publicKey.pem
        }
    }


    @Test
    public void test2() throws Exception {
        String privateKey = "4bfda50f21e540f19ab394218e62ccb7_privateKey.pem";
        RSAPrivateKey key = (RSAPrivateKey)RSAEncrypt.loadKeyFromFile(path + privateKey, RSAPrivateKey.class);
        byte[] encrypt = RSAEncrypt.encrypt(key, "123456ABc".getBytes());
        System.out.println(new String(Base64Utils.encode(encrypt)));

        String publicKey = "4bfda50f21e540f19ab394218e62ccb7_publicKey.pem";
        RSAPublicKey rsaPublicKey = (RSAPublicKey)RSAEncrypt.loadKeyFromFile(path + publicKey, RSAPublicKey.class);
        byte[] decrypt = RSAEncrypt.decrypt(rsaPublicKey, encrypt);
        System.out.println(new String(decrypt));

    }


    @Test
    public void test3() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        File file = new File(path + "4bfda50f21e540f19ab394218e62ccb7_privateKey.pem");
        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] keyBytes = null;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int len;
        byte[] buffer = new byte[1024];
        while ((len = fileInputStream.read(buffer)) != -1) {
            byteArrayOutputStream.write(buffer, 0, len);
        }
        keyBytes = byteArrayOutputStream.toByteArray();

        fileInputStream.close();
        byteArrayOutputStream.close();

        byte [] a = Base64.getDecoder().decode(new String(keyBytes));

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(a);
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);
        byte[] bytes = cipher.doFinal("plainTextData".getBytes());
        System.out.println(new String(bytes));
    }


    @Test
    public void test4() throws Exception {
        String privateKey = "4bfda50f21e540f19ab394218e62ccb7_privateKey.pem";
        RSAPrivateKey key = (RSAPrivateKey)RSAEncrypt.loadKeyFromFile(path + privateKey, RSAPrivateKey.class);
        String sign = RSAEncrypt.sign("123456AbCd".getBytes(), key);
        System.out.println(sign);

        String publicKey = "4bfda50f21e540f19ab394218e62ccb7_publicKey.pem";
        RSAPublicKey rsaPublicKey = (RSAPublicKey)RSAEncrypt.loadKeyFromFile(path + publicKey, RSAPublicKey.class);

        boolean verify = RSAEncrypt.verify("123456AbCd1".getBytes(), rsaPublicKey, sign);
        System.out.println(verify);
    }


}
