# geeking-common-utils

#### Presentation

A collection of Java common tools, Some code excerpted from other Open-Source-Project,
You can freely download and install to use when you need ! 


#### Excerpt From List：(Include but not limit，Not ranked)
1. [google.guava](https://github.com/google/guava) 
2. [aws-java-sdk](https://github.com/aws/aws-sdk-java) 
3. [apache-commons-lang](https://github.com/apache/commons-lang)


#### How to install
```shell script
$ git clone git@gitee.com:devilscode/geeking-common-utils.git
$ cd geeking-common-utils & mvn clean install
```


#### How to use in your maven project
```xml
<dependency>
    <groupId>com.mohist.simplify</groupId>
    <artifactId>common-lang-utils</artifactId>
    <version>${mohist.utils.version}</version>
</dependency>
```

#### Participants
You can't participate yet, maybe you can do this in the future, We accept contributions and issues.